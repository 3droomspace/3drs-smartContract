pragma solidity >=0.7.0 <0.9.0;
// SPDX-License-Identifier: GPL-3.0

contract ScoreKeeping {

    address private contractOwner;          // Account used to deploy contract
    bool private isOperational = true;

    //mapping for keeping tab of user scr
    mapping(string => int256) private userScore;

    //event when scr updated
    event ScoreUpdated(string user, int256 updatedScore);

    modifier requireIsOperational() {
         // Modify to call data contract's status
        require(isOperational, "Contract is currently not operational");
        _; 
    }

    modifier requireContractOwner() {
        require(msg.sender == contractOwner, "Caller is not contract owner");
        _;
    }

    /********************************************************************************************/
    /*                                       UTILITY FUNCTIONS                                  */
    /********************************************************************************************/

    function checkIsOperational() public view returns(bool) {
        return isOperational;  // Modify to call data contract's status
    }

    function setOperationalVal(bool _setOp) public requireContractOwner {
        require(_setOp != isOperational, "Both op and setOp vals are same");
        isOperational = _setOp;
    }

    /********************************************************************************************/
    /*                                       CONSTRUCTOR                                        */
    /********************************************************************************************/
    constructor() {
        contractOwner = msg.sender;
    }

    function updateScoreForUser(string memory _user, int256 _score) external requireIsOperational requireContractOwner {
        userScore[_user] = userScore[_user] + _score;
        emit ScoreUpdated(_user , userScore[_user]);
    }

    function fetchScoreForUser(string memory _user) public view returns(int256) {
        return userScore[_user];
    }

}

contract ThreeDRSBusinessLogic {

    // declaration of contract
    ScoreKeeping scoreKeeping;

    constructor(address _scoreKeeping) {
        //passing address to ScoreKeeping ref
        scoreKeeping = ScoreKeeping(_scoreKeeping);
    }
    
    function updateScoreForUser(string memory _user, int256 _score) public {
        scoreKeeping.updateScoreForUser(_user,_score);
    }

    function fetchScoreForUser(string memory _user) public view returns(int256) {
        return scoreKeeping.fetchScoreForUser(_user);
    }

}